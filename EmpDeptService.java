package com.relations.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.relations.model.Department;
import com.relations.model.Employee;
import com.relations.repository.DepartmentRepository;
import com.relations.repository.EmployeeRepository;

@Service
public class EmpDeptService {
	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	public List<Employee> getAllEmp() {
		return employeeRepository.findAll();
	}

	public Employee saveEmp(Employee employee) {
		return employeeRepository.save(employee);
	}

	public Department saveDept(Department department) {
		return departmentRepository.save(department);
	}

}
