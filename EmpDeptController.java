package com.relations.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.relations.model.Department;
import com.relations.model.Employee;
import com.relations.service.EmpDeptService;
@RestController
@Component
public class EmpDeptController {

		@Autowired
		EmpDeptService empDeptService;

		@GetMapping(value = "/employee", produces = { MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<List<Employee>> getEmployees(@RequestParam(name = "name", required = false) Integer age) {
			List<Employee> employee = empDeptService.getAllEmp();
			return ResponseEntity.status(200).body(employee);
		}

		@PostMapping("/employee1")
		public ResponseEntity<Employee> saveEmp(@RequestBody Employee employee) {
			return ResponseEntity.status(201).body(empDeptService.saveEmp(employee));
		}

		@PostMapping("/department")
		public ResponseEntity<Department> saveDept(@RequestBody Department department) {
			return ResponseEntity.status(201).body(empDeptService.saveDept(department));
		}
	}
	
