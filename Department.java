package com.relations.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.Id;

@Entity
@Table(name = "department")
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	int id;
	@Column
	String name;

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", list=" + list + "]";
	}

	@OneToMany
	private List<Employee> list = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String deptName) {
		this.name = deptName;
	}

	/*
	 * public List getEmployeelist() { return employeelist; }
	 * 
	 * public void setEmployeelist(List employeelist) { this.employeelist =
	 * employeelist; }
	 */
}
